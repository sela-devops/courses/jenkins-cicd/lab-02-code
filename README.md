# Creating a Freestyle Job
---

## Preparation

 - Ask the instructor for the server IP and credentials


## Instructions

 - Go to the freestyle folder

![Image 1](images/image-01.png)


 - Create a new job by click "New Item"
 
![Image 2](images/image-02.png)


 - Set the job name "freestyle-<your-name>" and select "Freestyle project"

![Image 3](images/image-03.png)


 - Configure the job to run in you slave

![Image 4](images/image-04.png)


 - Set the sources repository (git)
 
```
https://github.com/leonjalfon1/npm-demo-app.git
```

![Image 5](images/image-05.png)


 - Configure the build process

```
npm install
npm test
npm run build
```

![Image 6](images/image-06.png)


 - Set the job to achieve the build artifact (nodejs-demoapp.zip)

![Image 7](images/image-07.png)


 - Save the changes and click "Build Now"

![Image 8](images/image-08.png)


 - Inspect the build details

![Image 9](images/image-09.png)


 - Inspect the build artifacts and the build console

![Image 10](images/image-10.png)
 